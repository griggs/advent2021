﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Advent
{
    public static class Day3
    {
        public static void Process()
        {
            var lines = System.IO.File.ReadAllLines("./Input/Day3.txt");
            var directions = new List<Route>();

            //Part1
            var gamma = "";
            var epsilon = "";
            for (var i = 0; i < 12; i++)
            {
                var zero = 0;
                var one = 0;

                foreach (var line in lines)
                {
                    if(line[i] == '0')
                    {
                        zero += 1;
                    }
                    else
                    {
                        one += 1;
                    }
                }

                gamma += zero > one ? "0" : "1";
                epsilon += zero > one ? "1" : "0";

            }

            Console.WriteLine($"Gamma: {Convert.ToInt32(gamma,2)}");
            Console.WriteLine($"Epsilon: {Convert.ToInt32(epsilon, 2)}");
            Console.WriteLine($"Power: {Convert.ToInt32(gamma, 2) * Convert.ToInt32(epsilon, 2)}");

            //Part2
            var oxygen = "";
            var co2 = "";

            var finalO = "";
            var finalC = "";
            for (var i = 0; i < 12; i++)
            {
                var zero = 0;
                var one = 0;

                if (i == 0)
                {
                    foreach (var line in lines)
                    {
                        if (line[i] == '0')
                        {
                            zero += 1;
                        }
                        else
                        {
                            one += 1;
                        }
                    }
                }
                else
                {
                    foreach (var line in lines.Where(x=>x.StartsWith(oxygen)))
                    {
                        if (line[i] == '0')
                        {
                            zero += 1;
                        }
                        else
                        {
                            one += 1;
                        }
                    }
                }
                                
                if(zero > one)
                {
                    oxygen += "0";
                }
                else if(one > zero)
                {
                    oxygen += "1";
                }
                else
                {
                    oxygen += "1";
                }

                if (lines.Count(x => x.StartsWith(oxygen)) == 1)
                {
                    finalO = lines.Where(x => x.StartsWith(oxygen)).FirstOrDefault();
                    break;
                }

            }

            for (var i = 0; i < 12; i++)
            {
                var zero = 0;
                var one = 0;

                if (i == 0)
                {
                    foreach (var line in lines)
                    {
                        if (line[i] == '0')
                        {
                            zero += 1;
                        }
                        else
                        {
                            one += 1;
                        }
                    }
                }
                else
                {
                    foreach (var line in lines.Where(x => x.StartsWith(co2)))
                    {
                        if (line[i] == '0')
                        {
                            zero += 1;
                        }
                        else
                        {
                            one += 1;
                        }
                    }
                }

                if (zero > one)
                {
                    co2 += "1";
                }
                else if (zero < one)
                {
                    co2 += "0";
                }
                else
                {
                    co2 += "0";
                }

                if (lines.Count(x => x.StartsWith(co2)) == 1)
                {
                    finalC = lines.Where(x => x.StartsWith(co2)).FirstOrDefault();
                    break;
                }

            }

            Console.WriteLine($"Oxygen: {Convert.ToInt32(finalO, 2)}");
            Console.WriteLine($"CO2: {Convert.ToInt32(finalC, 2)}");
            Console.WriteLine($"Life Support: {Convert.ToInt32(finalO, 2) * Convert.ToInt32(finalC, 2)}");

        }

    }

    
}
