﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Advent
{
    public static class Day4
    {
        public static void Process()
        {
            var lines = System.IO.File.ReadAllLines("./Input/Day4.txt");
            var numbers = lines.First().Split(",");
            var boards = CreateBoards(lines);
                        
            //Part1
            foreach(var number in numbers)
            {
                foreach(var board in boards)
                {
                    board.CalledNumber(number);
                    if(board.HasBingo())
                    {
                        Console.WriteLine($"Part 1: {board.Score() * int.Parse(number)}");
                        goto Break1;
                    }
                }

            }

        Break1:
            Console.WriteLine("");

            //Part 2
            boards.ForEach(x => x.ClearBoard());
            var winners = 0;
            foreach (var number in numbers)
            {   
                foreach (var board in boards)
                {
                    if(!board.HasBingo())
                    {
                        board.CalledNumber(number);
                        if (board.HasBingo())
                        {
                            winners += 1;
                            if (winners == boards.Count())
                            {
                                Console.WriteLine($"Part 2: {board.Score() * int.Parse(number)}");
                                goto Break2;
                            }
                        }
                    }
                    
                }

            }

        Break2:
            Console.WriteLine("");

        }

        static List<Board> CreateBoards(string[] lines)
        {
            var boards = new List<Board>();

            for(var i=1;i<lines.Length;i+=6)
            {
                var board = new Board() { Numbers = new Number[5,5]};

                for(var x = 1;x<=5;x++)
                {
                    var numbers = new List<string>();
                    numbers.Add(String.Concat(lines[i + x].Take(2)));
                    numbers.Add(String.Concat(lines[i + x].Skip(3).Take(2)));
                    numbers.Add(String.Concat(lines[i + x].Skip(6).Take(2)));
                    numbers.Add(String.Concat(lines[i + x].Skip(9).Take(2)));
                    numbers.Add(String.Concat(lines[i + x].Skip(12).Take(2)));

                    for (var y = 0; y < 5; y++)
                    {
                        var number = new Number() { Value = numbers[y] };
                        board.Numbers[x - 1, y] = number;
                    }
                    
                }

                boards.Add(board);
                
            }

            return boards;
        }

    }

    class Board
    {
        public Number[,] Numbers { get; set; }

        public void ClearBoard()
        {
            for (var x = 0; x < 5; x++)
            {
                for (var y = 0; y < 5; y++)
                {
                    Numbers[x, y].Called = false;                    
                }
            }
        }

        public int Score()
        {
            var score = 0;

            for (var x = 0; x < 5; x++)
            {
                for (var y = 0; y < 5; y++)
                {
                    if (Numbers[x, y].Called == false)
                    {
                        score += int.Parse(Numbers[x, y].Value);
                    }
                }
            }

            return score;
        }

        public void CalledNumber(string number)
        {
            for(var x =0;x<5; x++)
            {
                for (var y = 0; y < 5; y++)
                {
                    if(Numbers[x,y].Value.Trim() == number.Trim())
                    {
                        Numbers[x, y].Called = true;
                    }
                }
            }
        }

        public bool HasBingo()
        {
            for(var x = 0;x<5;x++)
            {
                if(Numbers[x,0].Called && Numbers[x, 1].Called && Numbers[x, 2].Called && Numbers[x, 3].Called && Numbers[x, 4].Called)
                {
                    return true;
                }
                if(Numbers[0,x].Called && Numbers[1,x].Called && Numbers[2,x].Called && Numbers[3,x].Called && Numbers[4,x].Called)
                {
                    return true;
                }
            }

            return false;
        }
    }
    class Number 
    {
        public string Value { get; set; }
        public bool Called { get; set; } = false;
    }


    
}
