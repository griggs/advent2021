﻿using System;

namespace Advent
{
    class Program
    {
        static void Main(string[] args)
        {
            Day4.Process();
        }
    }
}
