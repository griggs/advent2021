﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Advent
{
    class Route
    {
        public Direction Direction { get; set; }
        public int Speed { get; set; }
    }
    enum Direction
    {
        Forward =1,
        Down =2,
        Up =3
    }
    public static class Day2
    {
        public static void Process()
        {
            var lines = System.IO.File.ReadAllLines("./Input/Day2-1.txt");
            var directions = new List<Route>();
            foreach(var line in lines)
            {
                var split = line.Split(" ");
                directions.Add(new Route() { Direction = split[0] == "forward" ? Direction.Forward : split[0] == "down" ? Direction.Down : Direction.Up, Speed = int.Parse(split[1]) });
            }

            //Part1
            var h = 0;
            var d = 0;
            foreach(var direction in directions)
            {
                switch(direction.Direction)
                {
                    case Direction.Forward:
                        h += direction.Speed;
                        break;
                    case Direction.Down:
                        d += direction.Speed;
                        break;
                    case Direction.Up:
                        d -= direction.Speed;
                        break;
                }
            }

            Console.WriteLine($"Part 1: {h * d}");

            //Part 2
            h = 0;
            d = 0;
            var a = 0;

            foreach (var direction in directions)
            {
                switch (direction.Direction)
                {
                    case Direction.Forward:
                        h += direction.Speed;
                        d += a * direction.Speed;
                        break;
                    case Direction.Down:
                        a += direction.Speed;
                        break;
                    case Direction.Up:
                        a -= direction.Speed;
                        break;
                }
            }

            Console.WriteLine($"Part 2: {h * d}");


        }

    }

    
}
