﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Advent
{
    public static class Day1
    {
        public static void Process()
        {
            var lines = System.IO.File.ReadAllLines("./Input/Day1-1.txt");
            
            //Part1
            var increase = 0;
            var previous = String.Empty;
            foreach(var line in lines)
            {
                if(!string.IsNullOrEmpty(previous) && int.Parse(line) > int.Parse(previous))
                {
                    increase++;
                }

                previous = line;
            }

            Console.WriteLine($"Part 1: {increase}");

            //Part2
            increase = 0;
            for(var i = 0; i<lines.Length;i++)
            {
                //Check if we can make comparison
                if(i+3 > lines.Length-1)
                {
                   break;
                }
                var range1 = int.Parse(lines[i]) + int.Parse(lines[i + 1]) + int.Parse(lines[i + 2]);
                var range2 = int.Parse(lines[i + 1]) + int.Parse(lines[i + 2]) + int.Parse(lines[i + 3]);

                if(range2 > range1)
                {
                    increase++;
                }
            }

            Console.WriteLine($"Part 2: {increase}");
        }

    }

    
}
